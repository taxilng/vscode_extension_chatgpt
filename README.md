# 环境配置

1. 安装依赖

```bash
yarn
```

2. 本地开发运行

键盘 F5

3. 打包成 VSIX 插件

```bash
npm i vsce -g

vsce package --no-dependencies
```

# 文档相关

1. [中文文档](https://liiked.github.io/VS-Code-Extension-Doc-ZH/#/api/README)
1. [官方英文文档](https://code.visualstudio.com/api)
1. [官方示例 demo](https://github.com/microsoft/vscode-extension-samples)

## 一些 API 说明

1. 上方正中位置弹出输入框

```js
const value = await vscode.window.showInputBox({
	prompt: "Ask anything...",
})
```

2. 获取选中的文本

```js
const editor = vscode.window.activeTextEditor
const selection = editor.document.getText(editor.selection)
```

3. 右下角弹窗以及回调

```js
vscode.window.showInformationMessage("请继续").then(async choice => {
	if (choice === "继续") {
		this.sendApiRequest("Continue", {
			command: options.command,
			code: undefined,
			previousAnswer: this.response,
		})
	}
})
```

## 主流程梳理

1. package.json编写右击事件

```json
"contributes": {
		"menus": {
			"editor/context": [
				{
					"submenu": "chatgpt/editor/context/menuItems",
					"group": "navigation",
					"when": "editorHasSelection"
				},
			],
			"chatgpt/editor/context/menuItems": [
				{
					"command": "vscode-chatgpt.whyBroken",
					"group": "chatgpt-menu-group1@1"
				},
				{
					"command": "vscode-chatgpt.askChatGptToExplainCode",
					"group": "chatgpt-menu-group1@2"
				}
			]
		},
		"submenus": [
			{
				"id": "chatgpt/editor/context/menuItems",
				"label": "🚀ChatGPT"
			}
		],
		"commands": [
			{
				"command": "vscode-chatgpt.whyBroken",
				"title": "检查下面代码是否有BUG并给出修复建议 🐛"
			},
			{
				"command": "vscode-chatgpt.askChatGptToExplainCode",
				"title": "详细讲解下面代码 💬"
			},
		],
	},

```

2. extension.ts文件写绑定右击事件
```typescript
const whyBroken = vscode.commands.registerCommand("vscode-chatgpt.whyBroken", async () => {
		const editor = vscode.window.activeTextEditor;
		const prompt = vscode.workspace.getConfiguration("chatgpt").get<string>(`promptPrefix.whyBroken`);
		if (!editor) {
			return;
		}
		const selection = editor.document.getText(editor.selection);
		if (selection && prompt) {
			provider?.sendApiRequest(prompt, { command: 'whyBroken', code: selection, language: editor.document.languageId });
		}
	});

```

3. chatgpt-view-provider.ts 文件写 插入文件和调用接口的方法

4. main.js 实现真正的html渲染，插入聊天文本